package com.ocadotechnology.aedlocator.model;

import lombok.RequiredArgsConstructor;
import lombok.Value;

@RequiredArgsConstructor
@Value

public class AedLocalizationDetails {
    private final double latitude;
    private final double longitude;
    private final LocationStatus locationStatus;
}
