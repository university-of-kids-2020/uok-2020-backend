package com.ocadotechnology.aedlocator.model;

public enum LocationStatus {
    VERIFICATION_PENDING,
    VERIFIED,
}
