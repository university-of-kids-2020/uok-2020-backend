package com.ocadotechnology.aedlocator;

import com.ocadotechnology.aedlocator.model.AedLocalizationDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/aed")
public class AedLocatorController {

    @GetMapping("/locations")
    public List<AedLocalizationDetails> getAllAedLocations() {
        return Collections.emptyList();
    }
}
